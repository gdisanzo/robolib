
mixin RoboInputMixin {

  String request;
  List<int> contextIds;

//  @override
  Map<String, dynamic> asMap() {
    return {
      'request' : request,
      'contextIds' : contextIds,
    };
  }

//  @override
  void readFromMap(Map<String, dynamic> object) {
    request = object['request'] as String;
    contextIds = object['contextIds'] as List<int>;
  }
}

mixin RoboOutputMixin {
  String response;
  RoboAction action;

//  @override
  Map<String, dynamic> asMap() {
    final result = <String, dynamic>{
      'response' : response
    };
    if(action != null){
      final actionMap = <String, dynamic>{
        'id' : action.id,
        'name' : action.name
      };
      if(action.params != null){
        actionMap['params'] = action.params;
      }
      result['action'] = actionMap;
    }
    return result;
  }

//  @override
  void readFromMap(Map<String, dynamic> object) {
    response = object['response'] as String;
    if(object['action'] != null){
      final actionMap = object['action'] as Map<String, dynamic>;
      action = RoboAction(actionMap['id'] as int);
      var tmp = actionMap['name'];
      if(tmp != null){
        action.name = tmp as String;
      }
    }
  }
}

class RoboAction {
  RoboAction(this.id);

  final int id;
  String name;
  Map<String, dynamic> params;
}

class MemDataMixin {
  int slot;
  String value;
  int millis;
  int active;
  int seq;

  //  @override
  Map<String, dynamic> asMap() {
    final m = {
      'slot' : slot,
      'value' : value,
      'millis' : millis,
      'active' : active,
    };
    if(seq != null)
      m['seq'] = seq;
    return m;
  }

//  @override
  void readFromMap(Map<String, dynamic> object) {
    slot = object['slot'] as int;
    value = object['value'] as String;
    millis = object['millis'] as int;
    active = object['active'] as int;
    seq = object['seq'] as int;
  }

}
