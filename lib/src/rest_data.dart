
class ServiceResult {

  static const String CODE_OK = 'OK';
  static const String CODE_NO = 'NO';
  static const String CODE_ERROR = 'ER';

  ServiceResult.empty();

  ServiceResult.fromMap(Map<String, dynamic> map){
    code = map['code'] as String;
    description = map['description'] as String;
  }

  ServiceResult.ok(this.description){
    code = CODE_OK;
  }

  ServiceResult.no(this.description){
    code = CODE_NO;
  }

  ServiceResult.error(this.description){
    code = CODE_ERROR;
  }

  ServiceResult(this.code, this.description);

  String code;
  String description;

  bool get success => code == CODE_OK;

  bool get failure => code != CODE_OK;

  Map<String, dynamic> asMap() {
    return {
      'code' : code,
      'description' : description,
    };
  }

}

class ResendMailResult extends ServiceResult {

  static const String CODE_BLOCKED = 'BLOCKED';
  // Reivio mail bloccato a tempo indeterminato
  static const String CODE_LOCKED = 'LOCKED';

  ResendMailResult.fromMap(Map<String, dynamic> map) : super.fromMap(map) {
    if(blocked){
      blockedUntil = DateTime.fromMillisecondsSinceEpoch(map['blockedUntil'] as int);
    }
  }

  ResendMailResult.tooMuchRetries(this.blockedUntil) : super(CODE_BLOCKED, "Troppi reivii mail. Riprovare dopo $blockedUntil");

  ResendMailResult.locked() : super(CODE_LOCKED, "Bloccato invio a tempo indeterminato");

  ResendMailResult.ok() : super.ok("OK");

  ResendMailResult.error(String description) : super.error(description);

  bool get blocked => code == CODE_BLOCKED;
  bool get locked => code == CODE_LOCKED;

  DateTime blockedUntil;

  Map<String, dynamic> asMap() {
    final map = super.asMap();
    if(blocked) {
      map.addAll({
        'blockedUntil': blockedUntil.millisecondsSinceEpoch,
      });
    }
    return map;
  }

}

class ChangePwdResult extends ServiceResult {

  static const String CODE_WRONG_PWD = 'WRONG_PWD';

  ChangePwdResult.fromMap(Map<String, dynamic> map) : super.fromMap(map);

  ChangePwdResult.wrongPwd() : super(CODE_WRONG_PWD, "Password errata");

  ChangePwdResult.ok() : super.ok("OK");

  ChangePwdResult.error(String description) : super.error(description);

  bool get wrongPwd => code == CODE_WRONG_PWD;

}